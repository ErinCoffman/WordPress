<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'database_name_here' );

/** Database username */
define( 'DB_USER', 'username_here' );

/** Database password */
define( 'DB_PASSWORD', 'password_here' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x=Sr{6D6l>m}5ZI&o|~&sRYV4qJ<f(l_ZMmu-u>Xv:SO|iPA-PZmfQMmn^s%~J+}');
define('SECURE_AUTH_KEY',  'S u+xJwS-rNEKBjUHWFY:Jo+[yK7X~@_injZ4EVoYkKV>BbYqDDa!|DCXGY,MLh%');
define('LOGGED_IN_KEY',    'Tx=`0<2wUzsT[<^`J3G1g+V0~J/LOg1^]ULWWc&Nxc:zXq Qt+91*WPN$=iG146Q');
define('NONCE_KEY',        '?_uV`86bS$8y-eX.M-%]CW y3j0gi Pa9V465;6S;/DO(`w.fJ{~_vuA0EY&56Ex');
define('AUTH_SALT',        'JA?x54}_sTZ(sDF9%#R+pU~R U+D=im+^/2a+L{B:`%Q&7zcI-0Lp$yTgaiumBa1');
define('SECURE_AUTH_SALT', 'z8|/^09LJYN-8Rw5yAR=coAtE6aC<  E6Mnc{k!&hz[sFaz|Q6gsbxg%n>kqZS.J');
define('LOGGED_IN_SALT',   'NE*fLjH(:r87*s:1Fv8gVx?ZC!y&lm|)?v@&@E{K:Pm~*6wtxCB6A?:1MN_(o:tU');
define('NONCE_SALT',       'l<9^%H|~3Bbq5rZCfQf;y^BxN;Q`R2Y5-(4LA&mFQmt: 2qvFVs(0j39S[D58CZ>');

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */
define( 'MULTISITE', true );
define( 'SUBDOMAIN_INSTALL', true );
define( 'DOMAIN_CURRENT_SITE', 'wordpress-emcoff.apps.cloudapps.unc.edu' );
define( 'PATH_CURRENT_SITE', '/' );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
